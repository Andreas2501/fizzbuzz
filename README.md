Lesson in abstraction
http://dave.fayr.am/posts/2012-10-4-finding-fizzbuzz.html moved...
https://github.com/KirinDave/public-website/blob/master/posts/2012-10-4-finding-fizzbuzz.markdown

FizzBuzz - Task:
"Write a program that prints the numbers from 1 to 100. But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”. For numbers which are multiples of both three and five print “FizzBuzz”."

Solving FizzBuzz with Datatypes upholding the laws of:

https://en.wikipedia.org/wiki/Associative_property
