#scala
http://mvnrepository.com/artifact/org.scalaz/scalaz-core_2.9.2

def f(mod:Int) (name:String)(value:Int) = if(value % mod == 0) Some(name) else None

val fizz=f(3)("Fizz") _

val buzz=f(5)("Buzz") _

val bazz=f(7)("Bazz") _

val functions=fizz |+| buzz |+| bazz

(1 to 100).map(x=> functions(x).getOrElse(""+x)).foreach(println)