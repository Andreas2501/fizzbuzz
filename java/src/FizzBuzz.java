import static fj.Semigroup.*;

import java.util.ArrayList;

import fj.F;
import fj.F2;
import fj.P2;
import fj.Semigroup;
import fj.Show;
import fj.data.List;
import fj.data.Option;

//requires FunctionalJava ! as a lib
public class FizzBuzz {

	public static void main(String[] args) {
		java.util.List<P2<Integer, String>> ll =simpleBazzEF();
		
		//simpleBazz();
		
//		for (P2<Integer, String> p2 : ll) {
//			System.out.println(p2._1()+" "+p2._2());
//		}
		
		//simpleC();
		ctBazz();
	}
	
	public static void terse(){
		for (int i = 0; i < 100; System.out.println(++i % 3 == 0 ? i % 5 == 0 ? "Fizzbuzz" : "Fizz" : i % 5 == 0 ? "Buzz" : i));
	}
	
	public static void simpleC(){
		for (int i = 1; i <= 100; i++)
	    {
	        if (i % 15 == 0)
	            System.out.println("FizzBuzz");
	        else if (i % 3 == 0)
	        	System.out.println("Fizz");
	        else if (i % 5 == 0)
	        	System.out.println("Buzz");
	        else
	        	System.out.println(i);
	    }
	}
	
	public static void simpleBazz(){
		for (int i = 1; i <= 100; i++)
	    {
	        if (i % 35 == 0)
	            System.out.println("BuzzBazz");
	        else if (i % 21 == 0)
	            System.out.println("FizzBazz");
	        else if (i % 15 == 0)
	            System.out.println("FizzBuzz");
	        else if (i % 3 == 0)
	        	System.out.println("Fizz");
	        else if (i % 5 == 0)
	        	System.out.println("Buzz");
	        else if (i % 7 == 0)
	        	System.out.println("Bazz");
	        else
	        	System.out.println(i);
	    }
	}
	
	public static P2<Integer, String> tup(final Integer i,final String s){
		return new P2<Integer, String>() {
			
			@Override
			public String _2() {
				return s;
			}
			
			@Override
			public Integer _1() {
				return i;
			}
		};
	}
	
	public static java.util.List<P2<Integer, String>> simpleBazzEF(){
		java.util.List<P2<Integer, String>> ll =new ArrayList<P2<Integer, String>>();
		
		for (int i = 1; i <= 100; i++)
	    {
	        if (i % 35 == 0)
	            ll.add(tup(i,"BuzzBazz"));
	        else if (i % 21 == 0)
	        	ll.add(tup(i,"FizzBazz"));
	        else if (i % 15 == 0)
	        	ll.add(tup(i,"FizzBuzz"));
	        else if (i % 3 == 0)
	        	ll.add(tup(i,"Fizz"));
	        else if (i % 5 == 0)
	        	ll.add(tup(i,"Buzz"));
	        else if (i % 7 == 0)
	        	ll.add(tup(i,"Bazz"));
	        else
	        	ll.add(tup(i,""+i));
	    }
		
		return ll;
	}
	
	public static void ctBazz(){
		   Option<String> a=Option.some("6");
           //Option<String> b=Option.none();
           Option<String> b=Option.some("8");
          
           //Semigroup<String> ss=Semigroup.stringSemigroup;
          
           //Semigroup<Option<String>> sg=optionSemigroup();
          
//           Option<String> r=sg.sum(a, b);
//          
//           System.out.println(r.isSome());
//           System.out.println(Show.optionShow(Show.stringShow).showS(r));
          
           for (int i = 1; i < 101; i++) {
                   System.out.println(emit().f(i).orSome(""+i));
           }
   }
  
   public static F<Integer, Option<String>> emit(){
         Semigroup<F<Integer, Option<String>>> sg=functionSemigroup(optionSemigroupDeeper(stringSemigroup));
          
		List<F<Integer, Option<String>>> functions=List.nil();
		
		functions=functions.
		cons(on(5,"Buzz")).
		cons(on(7,"Bazz"));
           
        return functions.foldLeft(sg.sum(), on(3,"Fizz"));
   }
  
    public static <A> Semigroup<Option<A>> optionSemigroupDeeper(final Semigroup<A> deeper) {
               return Semigroup.semigroup(new F2<Option<A>, Option<A>, Option<A>>() {
                 public Option<A> f(final Option<A> a1, final Option<A> a2) {
                      if(a1.isSome() && a2.isSome())
                              return Option.some(deeper.sum(a1.some(), a2.some()));
                      else
                          return a1.isSome() ? a1 : a2;
                 }
               });
   }
    
   public static F<Integer, Option<String>> on(final int mod , final String name){
           return new F<Integer, Option<String>>() {
                  
                   @Override
                   public Option<String> f(Integer arg0) {
                           if(arg0 % mod == 0)
                                   return Option.some(name);
                           else
                                   return Option.none();
                   }
           };
   }
	

}



