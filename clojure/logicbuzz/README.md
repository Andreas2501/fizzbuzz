# Logic Buzz

A different take on a classic.

## Usage

Using Leiningen, just run: `lein run`

## License

Copyright © 2014 Joshua Ballanco

Distributed under the BSD 2-Clause License. See COPYING for full details.

