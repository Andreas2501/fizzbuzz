<Query Kind="Program" />


  public interface Semigroup<A>
        {
            A append(A a1, A a2);
        }

        public interface Monoid<A> : Semigroup<A>
        {
            /// <summary>
            /// Identity
            /// <summary>
            A empty();

            // Concat is defined in LanguageExt.TypeClass
        }

        public class StringSemigroup : Semigroup<string> {
            public string append(string x, string y) {
                return x + y;
            }
        }

        public class IntMulMonoid : Monoid<int> {

            public int empty() {
                return 1;
            }

            public int append(int x, int y)
            {
                return x * y;
            }

        }

        public class IntPlusMonoid : Monoid<int>
        {

            public int empty()
            {
                return 0;
            }

            public int append(int x, int y)
            {
                return x + y;
            }

        }

 
 public class Program
    {

        static void Main(string[] args)
        {
 			List<string> sl = new List<string>() {"a","b","c" };

            List<int> nl = new List<int>() { 3, 2, 1 };

            Console.WriteLine(sl.Aggregate(new StringSemigroup().append));
            Console.WriteLine(nl.Aggregate(new IntPlusMonoid().append));
            
			IntMulMonoid imulmonoid=new IntMulMonoid();
			
			Console.WriteLine(nl.Aggregate(imulmonoid.empty(),imulmonoid.append));
            
        }
    }