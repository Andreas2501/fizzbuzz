﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{

    //https://github.com/NICTA/xsharpx/blob/master/src/XSharpx/Option.cs
    public struct Option<A>
    {
        private readonly bool ne;
        private readonly A a;

        private Option(bool e, A a)
        {
            this.ne = !e;
            this.a = a;
        }

        public bool IsEmpty
        {
            get
            {
                return !ne;
            }
        }

        public static Option<A> None
        {
            get
            {
                return new Option<A>(true, default(A));
            }
        }

        public static Option<A> Some(A t)
        {
            return new Option<A>(false, t);
        }

        public A Value()
        {
            return a;
        }

        public A orSome(A a)
        {
            return IsEmpty == false ? Value() : a;
        }

    }

    //https://github.com/functionaljava/functionaljava/blob/master/core/src/main/java/fj/Semigroup.java
    public class Semigroup<A>
    {
        private Func<A, Func<A, A>> appendF;

        private Semigroup(Func<A, Func<A, A>> appendF)
        {
            this.appendF = appendF;
        }

        public static Semigroup<A> semigroup(Func<A, A, A> op)
        {
            return new Semigroup<A>(FuncFns.Curry(op));
        }

        public A append(A a1, A a2)
        {
            return appendF.Invoke(a1).Invoke(a2);
        }


        public Func<A, Func<A, A>> append()
        {
            return appendF;
        }

        public static Semigroup<string> stringSemigroup = Semigroup<string>.semigroup((s1, s2) => s1 + s2);



        public static Semigroup<Func<X, Y>> functionSemigroup<X, Y>(Semigroup<Y> sb)
        {
            return Semigroup<Func<X, Y>>.semigroup((a1, a2) => a => sb.append(a1.Invoke(a), a2.Invoke(a)));
        }


        public static Semigroup<Option<X>> optionSemigroupDeeper<X>(Semigroup<X> deeper)
        {
            Func<Option<X>, Option<X>, Option<X>> f2 = (a1, a2) =>
               {
                   if (a1.IsEmpty == false && a2.IsEmpty == false)
                       return Option<X>.Some(deeper.append(a1.Value(), a2.Value()));
                   else
                       return a1.IsEmpty == false ? a1 : a2;
               };

            return Semigroup<Option<X>>.semigroup(f2);
        }
    }

    public struct FuncFns
    {

        public static Func<A, Func<B, C>> Curry<A, B, C>(Func<A, B, C> f)
        {
            return a => b => f(a, b);
        }

    }

    public class Program
    {

        public static Func<int, Option<string>> on(int mod, string name)
        {
            return (arg0) =>
            {
                if (arg0 % mod == 0)
                    return Option<string>.Some(name);
                else
                    return Option<string>.None;
            };
        }


        public static Func<int, Option<string>> emit()
        {
            Semigroup<Func<int, Option<string>>> sg = Semigroup<Func<int, Option<string>>>.functionSemigroup<int,Option<string>>(
                                                                                Semigroup<Option<string>>.optionSemigroupDeeper(
                                                                                    Semigroup<string>.stringSemigroup));

            List<Func<int, Option<string>>> functions = new List<Func<int, Option<string>>>();

            functions.Add(on(5, "Buzz"));
            functions.Add(on(7, "Bazz"));

            return functions.Aggregate(on(3, "Fizz"), sg.append);
        }


        static void Main(string[] args)
        {
            for(int i = 1; i < 101; i++) {
                Console.WriteLine(emit().Invoke(i).orSome("" + i));
            }

            Console.ReadLine();
        }
    }
}
