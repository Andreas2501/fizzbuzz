//http://applicative-errors-scala.googlecode.com/svn/artifacts/0.6/html/index.html#Validation.cs
//https://gist.github.com/ashmind/866786
//http://lukepalmer.wordpress.com/2010/02/05/associative-alpha-blending/
void Main()
{
	
	var ls=new List<int>();
	ls.Add(1);
	ls.Add(2);
	ls.Add(3);
	ls.Add(4);
	
	var l2s=new List<int>();
	l2s.Add(4);	
	
	var l3s=new List<List<int>>();	
	
	ls.Aggregate(intAdditionMonoid.Zero, intAdditionMonoid.Operation).Dump();
	ls.Aggregate(intMultiplicationMonoid.Zero, intMultiplicationMonoid.Operation).Dump();
}

//What does Semigroup represent?
public interface Semigroup<A> {
  A append(A a1, A a2);    
}

public struct IntAddSemigroup : Semigroup<int> {
  public int append(int a, int b) {    
    return a + b;
  }    
}

public struct IntMulSemigroup : Semigroup<int> {
  public int append(int a, int b) {    
    return a * b;
  }
  
  //public Func<int, int, int> semiF() {
  //  return (a, b) =>  a * b; // warum kann ichhier nicht append aufrufen?
 // }
}

 
public struct ListSemigroup<A> : Semigroup<List<A>> {
  public List<A> append(List<A> l1, List<A> l2) {
    var z = new List<A>(l1);
    z.AddRange(l2);
    return z;
  }
    
}

//how does Monoid differ from Semigroup
public class Monoid<A> {
    public A Zero { get; private set; }
    public Func<A, A, A> Operation { get; private set; }
 
    public Monoid(Func<A,A,A> operation, A zero) {
        this.Operation = operation;
        this.Zero = zero;
    }
	
	public Monoid(Semigroup<A> s, A zero) {
        this.Operation = s.append;
        this.Zero = zero;
    }
}

 Monoid<int> intAdditionMonoid = new Monoid<int>(new IntAddSemigroup().append, 0);
 Monoid<int> intMultiplicationMonoid = new Monoid<int>(new IntMulSemigroup().append, 1);
 
 Monoid<List<int>> listAppendMonoid = new Monoid<List<int>>(new ListSemigroup<int>().append, new List<int>());

